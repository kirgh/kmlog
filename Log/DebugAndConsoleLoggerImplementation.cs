﻿namespace Z.KM.Log
{
    public class DebugAndConsoleLoggerImplementation : ILoggerImplementation
    {
        private static DebugAndConsoleLoggerImplementation _instance;

        private DebugAndConsoleLoggerImplementation()
        {

        }

        public static ILoggerImplementation Instance
        {
            get
            {
                return _instance ?? (_instance = new DebugAndConsoleLoggerImplementation());
            }
        }

        public void Log(string s)
        {
            System.Diagnostics.Debug.WriteLine(s);
            System.Console.WriteLine(s);
        }

        public void LogFormat(string format, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(string.Format(format, args));
            System.Console.WriteLine(format, args);
        }

        public void Warning(string s)
        {
            Log("WARN: " + s);
        }

        public void Error(string s)
        {
            Log("ERROR: " + s);
        }
    }
}
