﻿namespace Z.KM.Log
{
    public interface ILoggerImplementation
    {
        void Log(string s);
        void LogFormat(string format, params object[] args);
        void Warning(string s);
        void Error(string s);
    }
}
