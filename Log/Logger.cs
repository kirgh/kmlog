﻿namespace Z.KM.Log
{
    public static class Logger
    {

        private static ILoggerImplementation _implementation = DebugLoggerImplementation.Instance;

        public static void SetImplementation(ILoggerImplementation value)
        {
            _implementation = value;
        }

        public static void Log(string s)
        {
            _implementation.Log(s);
        }

        public static void LogFormat(string format, params object[] args)
        {
            _implementation.LogFormat(format, args);
        }

        public static void Warning(string s)
        {
            _implementation.Warning(s);
        }

        public static void Error(string s)
        {
            _implementation.Error(s);
        }

        [System.Obsolete("Not implemented code")]
        public static void Todo()
        {
            Todo("not implemented");
        }

        [System.Obsolete("Not implemented code")]
        public static void TodoLowPriority(string s)
        {
        }

        [System.Obsolete("Not implemented code")]
        public static void Todo(string s)
        {
            Error("TODO: " + s);
        }

        [System.Obsolete("Code marked as error", true)]
        public static void StaticAssert(string v)
        {
            Error("code marked as error");
        }

        public static void Assert(bool value, string text)
        {
            if (!value)
            {
                Error("Assert: " + text);
            }
        }
    }
}
