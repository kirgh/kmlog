﻿namespace Z.KM.Log
{
    public class DebugLoggerImplementation : ILoggerImplementation
    {
        private static DebugLoggerImplementation _instance;

        private DebugLoggerImplementation()
        {

        }

        public static ILoggerImplementation Instance
        {
            get
            {
                return _instance ?? (_instance = new DebugLoggerImplementation());
            }
        }

        public void Log(string s)
        {
            System.Diagnostics.Debug.WriteLine(s);
        }

        public void LogFormat(string format, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(string.Format(format, args));
        }

        public void Warning(string s)
        {
            Log("WARN: " + s);
        }

        public void Error(string s)
        {
            Log("ERROR: " + s);
        }
    }
}
